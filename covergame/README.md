
How to launch

1. Open project in Intellij IDEA

2. Run GenSCV.scala to generate samples

3. Run Economist.java

4. Add command line arguments in idea settings:

![](https://i.imgur.com/2HHKx41.png)
![](https://i.imgur.com/oV9ZBzb.png)

5. Rerun Economist.java

