import economist.Product;
import sun.reflect.generics.tree.Tree;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.util.*;

public class Economist {

    private static final int threadNum = 8;
    private static final String usage = "Usage: ./cheapests <count> <folder with csvs>";

    private static TreeSet<Product> process(final List<String> files, int maxCount) throws Exception {
        TreeSet<Product> cheapests = new TreeSet<>();

        for (String fileName : files) {
            FileReader fileReader = new FileReader("data/" + fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] row = line.split(";");
                if (row.length == 5) {
                    Product product = new Product(
                            Integer.parseInt(row[0]),
                            row[1],
                            row[2],
                            row[3],
                            Float.parseFloat(row[4])
                    );

                    cheapests.add(product);
                    if (cheapests.size() > maxCount) {
                        cheapests.pollLast();
                    }
                }
            }

            bufferedReader.close();
        }

        return cheapests;
    }

    private static <T> TreeSet<T> joinSets(List<TreeSet<T>> sets, int max) {
        TreeSet<T> treeSet = new TreeSet<>();
        for (TreeSet<T> set : sets) {
            for (T entry: set) {
                treeSet.add(entry);
                if (treeSet.size() > max)
                    treeSet.pollLast();
            }
        }
        return treeSet;
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 2 || !args[0].matches("\\d+")) {
            System.out.println(usage);
            return;
        }

        final int leastNumber = Integer.parseInt(args[0]);
        final String folderName = args[1];

        final File file = new File(folderName);
        if (!file.exists()) {
            System.out.println(usage);
            return;
        }

        long start = System.currentTimeMillis();

        String[] csvFilesNames = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.matches(".*\\.csv");
            }
        });

        final List<String> files = Arrays.asList(csvFilesNames);
        final int step = files.size() / threadNum;
        final List<TreeSet<Product>> cheapests = Collections.synchronizedList(new ArrayList<>());
        final List<Thread> threads = new ArrayList<>();

        for (int i = 0; i+step <= files.size(); i += step) {
            final List<String> filesPart = files.subList(i, Math.min(files.size(), i+step));
            Thread thread = new Thread(() -> {
                try {
                    cheapests.add(
                        process(filesPart, leastNumber)
                    );
                } catch (Exception e) {}
            });
            thread.start();
            threads.add(thread);
        }

        for (Thread thread : threads) {
            thread.join();
        }

        TreeSet<Product> cheapest = joinSets(cheapests, leastNumber);

        System.out.println("Cheapests: ");
        cheapest.forEach(System.out::println);

        long finish = System.currentTimeMillis();
        System.out.println("Job finished in: " + (finish - start));
    }
}

