
import scala.util.Random
import java.io._

object GenCSV extends App {
    for {
        i <- 1 to 10000
        file = new File(s"data/$i.csv")
        bw = new BufferedWriter(new FileWriter(file))
    } {
        (for (j <- 1 to 10000)
            yield Seq(
                Random.nextInt(1000),
                Random.alphanumeric.take(10).mkString,
                Random.alphanumeric.take(10).mkString,
                Random.alphanumeric.take(10).mkString,
                Random.nextFloat(),
            ).mkString(";")).toStream.foreach { e =>
            bw.write(e)
            bw.newLine()
        }
        bw.close()
    }
}
